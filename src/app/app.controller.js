export default class AppController {
  constructor ($state) {
    this.state = $state;
  }

  /**
   * Returns true if the provided state name is the current one.
   *
   * @param {String} stateName Name of the state
   * @returns {Boolean} True if the provided state name is the current one.
   * @memberof AppController
   */
  isActiveItem (stateName) {
    return this.state.current.name === stateName;
  }
}

AppController.$inject = ['$state'];
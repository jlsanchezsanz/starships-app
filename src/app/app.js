/* global process */
import angular from 'angular';
import ngAnimate from 'angular-animate';
import ngAria from 'angular-aria';
import ngTouch from 'angular-touch';
import ngBootstrap from 'angular-ui-bootstrap';
import uiRouter from 'angular-ui-router';

import '../styles/main.css';
import 'bootstrap/dist/css/bootstrap.min.css';

import config from 'app.config';

import appConfig from './app.config';
import appRoute from './app.route';
import appComponent from './app.component';
import { StarshipsListComponent } from '../components/starships-list/starships-list.component';
import { StarshipDetailComponent } from '../components/starship-detail/starship-detail.component';
import { StarshipTunningComponent } from '../components/starship-tunning/starship-tunning.component';
import { ImgCarouselComponent } from '../components/img-carousel/img-carousel.component';
import { ToggleButtonComponent } from '../components/toggle-button/toggle-button.component';
import Services from '../services/';

export default angular.module('starships-app', [
  ngAnimate,
  ngAria,
  ngTouch,
  ngBootstrap,
  uiRouter,
  Services
])
.config(appConfig)
.config(appRoute)
.constant('CONFIG', config)
.constant('ENVIRONNEMENT', process.env.ENV_NAME)
.component('app', appComponent)
.component('starshipsList', StarshipsListComponent)
.component('starshipDetail', StarshipDetailComponent)
.component('starshipTunning', StarshipTunningComponent)
.component('imgCarousel', ImgCarouselComponent)
.component('toggleButton', ToggleButtonComponent)
.name;

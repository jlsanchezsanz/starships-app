/**
 * Application route definition.
 */
export default function ($stateProvider, $urlRouterProvider) {
  'ngInject';

  $stateProvider
    .state('app', {
      url      : '',
      component: 'app'
    })
    .state('app.ships-list', {
      url      : '/ships-list',
      component: 'starshipsList'
    })
    .state('app.ship-details', {
      url      : '/ship-details/:shipId',
      component: 'starshipDetail'
    })
    .state('app.ship-tunning', {
      url      : '/ship-tunning/:shipId',
      component: 'starshipTunning'
    });

  $urlRouterProvider.otherwise('/ships-list');

}

import StarshipsService from './starships.service';
import StarshipDetailService from './starship-detail.service';

export default angular.module('app.services', [])
.service('StarshipsService', StarshipsService)
.service('StarshipDetailService', StarshipDetailService)
.name;
export default class StarshipDetailService {
  /**
   * Creates an instance of StarshipDetailService.
   *
   * @param {Object} $http Angular http service.
   * @memberof StarshipDetailService
   */
  constructor ($http) {
    this.urlBase = 'https://swapi.co/api/starships/';
    this.http = $http;
  }

  /**
   * Returns a promise that when resolved will provide
   * the detail of the starship which id has been passed as argument.
   *
   * @returns {Promise} Promise that when resolved returns the detail of a starship.
   * @memberof StarshipDetailService
   */
  getStarshipDetail (shipId) {
    return this.http.get(`${this.urlBase}${shipId}`)
      .then((response) => response);
  }
}

StarshipDetailService.$inject = ['$http'];
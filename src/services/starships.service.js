export default class StarshipsService {
  /**
   * Creates an instance of StarshipsService.
   *
   * @param {Object} $http Angular http service.
   * @param {Object} $timeout Angular timeout service.
   * @memberof StarshipsService
   */
  constructor ($http, $timeout) {
    this.reset();
    this.http = $http;
    this.timeout = $timeout;
  }

  /**
   * Returns a promise that when resolved will provide the list of starships.
   *
   * @returns {Promise} Promise that when resolved returns the list of starships.
   * @memberof StarshipsService
   */
  getStarships () {
    if (this.endpoint) {
      return this.http.get(this.endpoint)
      .then((response) => {
        this.endpoint = response.data.next;
        return this.timeout(() => response.data);
      });
    }
  }

  /**
   * Resets the status of the service.
   *
   * @memberof StarshipsService
   */
  reset () {
    this.endpoint = 'https://swapi.co/api/starships';
  }
}

StarshipsService.$inject = ['$http', '$timeout'];
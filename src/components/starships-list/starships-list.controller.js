export default class StarshipsListController {
  /**
   * Creates an instance of StarshipsListController.
   * @param {Object} StarshipsService StarshipsService service
   * @param {Object} $state Angular $state service
   * @param {Object} $document Angular $document service
   * @memberof StarshipsListController
   */
  constructor (StarshipsService, $state, $document) {
    this.starshipsService = StarshipsService;
    this.state = $state;
    this.document = $document;
    this.reset();
    this.getStarships();
    this.handleScrollBottom();
  }

  /**
   * Detects if the scroll of an element has reached its bottom.
   *
   * @param {HTMLElement} element HTMLElement
   * @returns {Boolean} True if the bottom has been reached.
   * @memberof StarshipsListController
   */
  detectScrollBottom (element) {
    return element.offsetHeight + element.scrollTop >= element.scrollHeight;
  }

  /**
   * Requests the next list of starships to the StarshipsService.
   *
   * @memberof StarshipsListController
   */
  getStarships () {
    if (!this.loading) {
      this.loading = true;
      this.starshipsService.getStarships().then((starships) => {
        this.nextPage = starships.next;
        this.starships = this.starships.concat(starships.results);
        this.loading = false;
      });
    }
  }

  /**
   * Handles the scroll to the bottom of the list in order to
   * request more star ships.
   *
   * @memberof StarshipsListController
   */
  handleScrollBottom () {
    const listElement = this.document.find('.starships-list')[0];
    angular.element(listElement).bind('scroll', () => {
      if (this.detectScrollBottom(listElement) && this.nextPage) {
        this.getStarships();
      }
    });
  }

  /**
   * Resets the status of the controller.
   *
   * @memberof StarshipsListController
   */
  reset () {
    this.starships = [];
    this.loading = false;
    this.starshipsService.reset();
  }

  /**
   * Navigates to the state "ship-details" with the selected starship.
   *
   * @param {Object} starship Selected starship.
   * @memberof StarshipsListController
   */
  starshipSelected (starship) {
    const shipId = starship.url.split('/')[5];
    this.state.go('app.ship-details', { shipId });
  }
}

StarshipsListController.$inject = ['StarshipsService', '$state', '$document'];
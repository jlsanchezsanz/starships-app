import template from './starships-list.component.html';
import controller from './starships-list.controller.js';
import './starships-list.component.css';

export const StarshipsListComponent = {
  template,
  controller
};
export default class ImgCarouselController {
  /**
   * Creates an instance of ImgCarouselController.
   * @param {Object} $scope Angular $scope service.
   * @param {Object} $timeout Angular $timeout service.
   * @param {Object} $document Angular $document service.
   * @memberof ImgCarouselController
   */
  constructor ($scope, $timeout, $document) {
    $scope.$watch('$ctrl.images', () => {
      this.activeImageIndex = Math.trunc(this.images.length / 2);
      const imgCarousel = angular.element($document.find('.carousel')[0]);
      $timeout(() => {
        imgCarousel.scrollLeft = imgCarousel.scrollWidth / 2;
      });
    });
  }

  /**
   * Returns the name of the class for the image depending
   * on its position and if it's selected.
   *
   * @param {Number} index Image index in the list.
   * @returns {String} Name of the class.
   * @memberof ImgCarouselController
   */
  getImageClass (index) {
    return index === this.activeImageIndex
      ? 'active'
      : index < this.activeImageIndex
        ? index === this.activeImageIndex - 1 ? 'left last' : 'left'
        : index === this.activeImageIndex + 1 ? 'right first' : 'right';
  }

  /**
   * Sets the provided index as the active one.
   *
   * @param {Number} $index Image index in the list.
   * @memberof ImgCarouselController
   */
  setActiveImage ($index) {
    this.activeImageIndex = $index;
  }
}

ImgCarouselController.$inject = ['$scope', '$timeout', '$document'];
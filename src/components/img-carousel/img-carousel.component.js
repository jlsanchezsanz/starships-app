import template from './img-carousel.component.html';
import controller from './img-carousel.controller.js';
import './img-carousel.component.css';

export const ImgCarouselComponent = {
  bindings: {
    images: '<'
  },
  template,
  controller
};
import template from './toggle-button.component.html';
import controller from './toggle-button.controller.js';
import './toggle-button.component.css';

export const ToggleButtonComponent = {
  bindings: {
    checked : '<',
    onChange: '&'
  },
  transclude: true,
  template,
  controller
};
export default class ToggleButtonController {
  /**
   * Creates an instance of ToggleButtonController.
   * @param {Object} $scope Angular scope service.
   * @param {Object} $element Angular element service.
   * @memberof ToggleButtonController
   */
  constructor ($scope, $element) {
    $scope.$watch('$ctrl.checked', () => {
      this.onChange({ checked: this.checked });
    });

    $element.bind('click', () => {
      $scope.$apply(() => {
        this.checked = !this.checked;
      });
    });
  }
}

ToggleButtonController.$inject = ['$scope', '$element'];
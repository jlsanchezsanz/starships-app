export default class StarshipTunningController {
  /**
   * Creates an instance of StarshipTunningController.
   * @memberof StarshipTunningController
   */
  constructor () {
    this.passengers = 30;
    this.date = new Date();
  }

  /**
   * Handles enable detection toggle button changes.
   *
   * @param {Object} checked Value of the toggle button.
   * @memberof StarshipTunningController
   */
  onDetectionChange (/* checked */) {
    // TODO: Handle enable detection
  }

  /**
   * Handles turbo boost toggle button changes.
   *
   * @param {Object} checked Value of the toggle button.
   * @memberof StarshipTunningController
   */
  onTurboBoostChange (/* checked */) {
    // TODO: Handle turbo boost
  }
}

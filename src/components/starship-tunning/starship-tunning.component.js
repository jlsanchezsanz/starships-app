import template from './starship-tunning.component.html';
import controller from './starship-tunning.controller.js';
import './starship-tunning.component.css';

export const StarshipTunningComponent = {
  template,
  controller
};
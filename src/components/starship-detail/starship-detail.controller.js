export default class StarshipDetailController {
  constructor (StarshipDetailService, $state) {
    this.starshipDetailService = StarshipDetailService;
    this.state = $state;
    this.getStarshipDetail(this.state.params.shipId);
    this.images = this.getImages();
  }

  /**
   * Provides a list of images urls.
   *
   * @returns {Array} List of images urls.
   * @memberof StarshipDetailController
   */
  getImages () {
    return [
      'http://www.d20radio.com/main/wp-content/uploads/2016/09/Decimator-300x300.jpg',
      'http://www.d20radio.com/main/wp-content/uploads/2016/09/Decimator-300x300.jpg',
      'http://www.d20radio.com/main/wp-content/uploads/2016/09/Decimator-300x300.jpg',
      'http://www.d20radio.com/main/wp-content/uploads/2016/09/Decimator-300x300.jpg',
      'http://www.d20radio.com/main/wp-content/uploads/2016/09/Decimator-300x300.jpg',
      'http://www.d20radio.com/main/wp-content/uploads/2016/09/Decimator-300x300.jpg',
      'http://www.d20radio.com/main/wp-content/uploads/2016/09/Decimator-300x300.jpg'
    ];
  }

  /**
   * Requests the next list of starships to the StarshipDetailService.
   *
   * @memberof StarshipDetailController
   */
  getStarshipDetail (shipId) {
    this.starshipDetailService.getStarshipDetail(shipId).then((starship) => {
      this.starship = starship;
    });
  }

  /**
   * Navigates to the state "ship-tunning" with the selected ship.
   *
   * @memberof StarshipDetailController
   */
  onSelectClick () {
    this.state.go('app.ship-tunning', { shipId: this.state.params.shipId });
  }
}

StarshipDetailController.$inject = ['StarshipDetailService', '$state'];
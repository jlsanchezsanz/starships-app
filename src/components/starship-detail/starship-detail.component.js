import template from './starship-detail.component.html';
import controller from './starship-detail.controller.js';
import './starship-detail.component.css';

export const StarshipDetailComponent = {
  template,
  controller
};
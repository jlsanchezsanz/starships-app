import AppModule from '../../src/app/app';

describe('StarshipDetail', () => {
  let $rootScope, $state, $location, $compile;

  beforeEach(window.module(AppModule));

  beforeEach(inject(($injector) => {
    $rootScope = $injector.get('$rootScope');
    $state = $injector.get('$state');
    $location = $injector.get('$location');
    $compile = $injector.get('$compile');
  }));

  describe('Module', () => {
    it('starshipDetail component should be visible when navigates to "/ship-details/:shipId"', () => {
      $location.url('/ship-details/5');
      $rootScope.$digest();
      $state.current.component.should.equal('starshipDetail');
    });
  });
  
  describe('Controller', () => {
    let starshipDetailController, starshipDetailService, element;
  
    beforeEach(inject((StarshipDetailService) => {
      element = angular.element('<starship-detail></starship-detail>');
      $compile(element)($rootScope)      
      starshipDetailController = element.controller('starshipDetail');
      starshipDetailService = StarshipDetailService;
    }));

    it('should return a list of images urls when "getImages" is invoked', () => {
      const images = starshipDetailController.getImages();
      assert.ok(Array.isArray([]));
      assert.ok(images.length > 0);
    });

    it('should call starshipDetailService getStarshipDetail method when "getStarshipDetail" is invoked', () => {
      starshipDetailController.getStarshipDetail();
      const spy = chai.spy.on(starshipDetailService, 'getStarshipDetail');
      expect(spy).to.have.been.called;
    });

    it('should navigate to when click on select button', () => {
      const spy = chai.spy.on($state, 'go');
      $state.params.shipId = '15';
      starshipDetailController.onSelectClick();
      expect(spy).to.have.been.called.with('app.ship-tunning', {shipId: $state.params.shipId});
    });
  });
});

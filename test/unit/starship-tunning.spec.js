import AppModule from '../../src/app/app';

describe('StarshipTunning', () => {
  let $rootScope, $state, $location, $compile, $timeout, $document;

  beforeEach(window.module(AppModule));

  beforeEach(inject(($injector) => {
    $rootScope = $injector.get('$rootScope');
    $state = $injector.get('$state');
    $location = $injector.get('$location');
    $compile = $injector.get('$compile');
    $timeout = $injector.get('$timeout');
    $document = $injector.get('$document');
  }));

  describe('Module', () => {
    it('starshipTunning component should be visible when navigates to "/ship-tunning/:shipId"', () => {
      $location.url('/ship-tunning/5');
      $rootScope.$digest();
      $state.current.component.should.equal('starshipTunning');
    });
  });
  
  describe('Controller', () => {
    let starshipTunningController, element;
  
    beforeEach(() => {
      element = angular.element('<starship-tunning></starship-tunning>');
      $compile(element)($rootScope)      
      starshipTunningController = element.controller('starshipTunning');
    });

    it('should handle enable detection change', () => {
      // TODO: implement test when onDetectionChange is implemented
    });

    it('should handle turbo boost change', () => {
      // TODO: implement test when onTurboBoostChange is implemented
    });
  });
});

import AppModule from '../../src/app/app';

describe('App', () => {
  let $rootScope, $state, $location, $compile, $timeout;

  beforeEach(window.module(AppModule));

  beforeEach(inject(($injector) => {
    $rootScope = $injector.get('$rootScope');
    $state = $injector.get('$state');
    $location = $injector.get('$location');
    $compile = $injector.get('$compile');
    $timeout = $injector.get('$timeout');
  }));

  describe('Module', () => {
    it('starshipsList component should be visible when navigates to ""', () => {
      $location.url('');
      $rootScope.$digest();
      $state.current.component.should.equal('starshipsList');
    });
  });

  describe('Controller', () => {
    let appController;

    beforeEach(() => {
      const element = angular.element('<app></app>');
      $compile(element)($rootScope)
      appController = element.controller('app');
    });

    it('isActiveItem should return true if the state name passed as parameter is the current one', () => {
      $state.go('app.ships-list');
      $timeout(() => {
        assert.equal(appController.isActiveItem('app.ships-list'), true);
      });
    });
  });
});

import AppModule from '../../src/app/app';

describe('StarshipsList', () => {
  let $rootScope, $state, $location, $compile, $timeout, $document;

  beforeEach(window.module(AppModule));

  beforeEach(inject(($injector) => {
    $rootScope = $injector.get('$rootScope');
    $state = $injector.get('$state');
    $location = $injector.get('$location');
    $compile = $injector.get('$compile');
    $timeout = $injector.get('$timeout');
    $document = $injector.get('$document');
  }));

  describe('Module', () => {
    it('starshipsList component should be visible when navigates to ""', () => {
      $location.url('');
      $rootScope.$digest();
      $state.current.component.should.equal('starshipsList');
    });
  });
  
  describe('Controller', () => {
    let starshipsListController, starshipsService, element;
  
    beforeEach(inject((StarshipsService) => {
      element = angular.element('<starships-list></starships-list>');
      $compile(element)($rootScope)      
      starshipsListController = element.controller('starshipsList');
      starshipsService = StarshipsService;
    }));

    it('should call starshipsService getStarships method when "getStarships" is invoked', () => {
      starshipsListController.getStarships();
      const spy = chai.spy.on(starshipsService, 'getStarships');
      expect(spy).to.have.been.called;
    });
    
    it('should reset the status of the controller when "reset" is invoked', () => {
      starshipsListController.reset();
      const spy = chai.spy.on(starshipsService, 'reset');
      assert.deepEqual(starshipsListController.starships, []);
      assert.equal(starshipsListController.loading, false);
      expect(spy).to.have.been.called;
    });

    it('should navigate to the state "ship-details" with the selected starship when "starshipSelected" is invoked', () => {
      const spy = chai.spy.on($state, 'go');
      starshipsListController.starshipSelected({ url: 'https://swapi.co/api/starships/15/' });
      expect(spy).to.have.been.called.with('app.ship-details', {shipId: '15'});
    });
  });
});

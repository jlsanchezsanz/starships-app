import AppModule from '../../src/app/app';

describe('StarshipDetailService', () => {
  let $http, starshipDetailService;

  beforeEach(window.module(AppModule));

  beforeEach(inject(($injector, StarshipDetailService) => {
    $http = $injector.get('$http');
    starshipDetailService = StarshipDetailService;
  }));

  it('"getStarshipDetail" should call $http.get to get the details of a starship', () => {
    const spy = chai.spy.on($http, 'get');
    starshipDetailService.getStarshipDetail(15);
    expect(spy).to.have.been.called.with(starshipDetailService.urlBase + '15');
  });
});

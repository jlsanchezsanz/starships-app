import AppModule from '../../src/app/app';

describe('ImgCarousel', () => {
  let $rootScope, $compile;

  beforeEach(window.module(AppModule));

  beforeEach(inject(($injector) => {
    $rootScope = $injector.get('$rootScope');
    $compile = $injector.get('$compile');
  }));
  
  describe('Controller', () => {
    let imgCarouselController, element;
  
    beforeEach(() => {
      element = angular.element('<img-carousel></img-carousel>');
      $compile(element)($rootScope)      
      imgCarouselController = element.controller('imgCarousel');
      imgCarouselController.activeImageIndex = 3;
    });

    it('"getImageClass" should return "active" when the index is the active one', () => {
      const imageClass = imgCarouselController.getImageClass(3);
      assert.equal(imageClass, 'active')
    });
    
    it('"getImageClass" should return "left" when the index is lower than the active one', () => {
      const imageClass = imgCarouselController.getImageClass(1);
      assert.equal(imageClass, 'left')
    });

    it('"getImageClass" should return "left last" when the index is at the left of the active one', () => {
      const imageClass = imgCarouselController.getImageClass(2);
      assert.equal(imageClass, 'left last')
    });

    it('"getImageClass" should return "right" when the index is greater than the active one', () => {
      const imageClass = imgCarouselController.getImageClass(5);
      assert.equal(imageClass, 'right')
    });

    it('"getImageClass" should return "right first" when the index is at the right of the active one', () => {
      const imageClass = imgCarouselController.getImageClass(4);
      assert.equal(imageClass, 'right first')
    });

    it('"setActiveImage" should set as activeImageIndex the index passed as argument', () => {
      imgCarouselController.setActiveImage(5);
      assert.equal(imgCarouselController.activeImageIndex, 5);
    });
  });
});

import AppModule from '../../src/app/app';

describe('StarshipsService', () => {
  let $http, starshipsService;

  beforeEach(window.module(AppModule));

  beforeEach(inject(($injector, StarshipsService) => {
    $http = $injector.get('$http');
    starshipsService = StarshipsService;
  }));

  it('"getStarships" should call $http.get with the endpoint', () => {
    const spy = chai.spy.on($http, 'get');
    starshipsService.getStarships();
    expect(spy).to.have.been.called.with('https://swapi.co/api/starships');
  });

  it('"reset" should set the main endpoint', () => {
    starshipsService.reset();
    assert.equal(starshipsService.endpoint, 'https://swapi.co/api/starships');
  });
});
